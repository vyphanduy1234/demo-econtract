import { SettingController } from './../../models/setting';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { NzMarks } from 'ng-zorro-antd/slider';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
})
export class SettingComponent implements OnInit {
  @Input() isVisible = false;
  @Output() onApplySetting = new EventEmitter<any>();
  ngOnInit(): void {}
  disabledSwitch = false;

  constructor() {}

  handleOk(): void {
    console.log(this.settingController);
    this.isVisible = !this.isVisible;
    this.onApplySetting.emit(this.settingController);
    this.disabledSwitch = false;
  }

  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = !this.isVisible;
    this.disabledSwitch = false;

  }

  prepareCurrentSetting(currentSetting) {
    this.isVisible = true;
    console.log(currentSetting);

    this.settingController = currentSetting;
    this.marksT = {
      0: 'Top',
      300: `${this.settingController.top}`,
    };

    this.marksL = {
      0: 'Left',
      300: `${this.settingController.left}`,
    };

    this.marksH = {
      0: 'Height',
      300: `${this.settingController.height}`,
    };

    this.marksW = {
      0: 'Width',
      300: `${this.settingController.width}`,
    };
  }

  onSliderChange(type) {
    switch (type) {
      case 't':
        this.marksT = {
          0: 'Top',
          300: `${this.settingController.top}`,
        };
      case 'l':
        this.marksL = {
          0: 'Left',
          300: `${this.settingController.left}`,
        };
      case 'h':
        this.marksH = {
          0: 'Height',
          300: `${this.settingController.height}`,
        };
      case 'w':
        this.marksW = {
          0: 'Width',
          300: `${this.settingController.width}`,
        };
    }
  }

  marksT: NzMarks = {
    0: 'Top',
  };
  marksL: NzMarks = {
    0: 'Left',
  };
  marksW: NzMarks = {
    0: 'Width',
  };
  marksH: NzMarks = {
    0: 'Height',
  };

  fontController: any = {
    selectedFont: '',
    fontData: ['san-serif', 'tahoma','fantasy'],
    fontStyle: ['normal', 'oblique','italic'],
    fontChange: () => {},
  };
  top = 10;

  settingController: SettingController = {
    id: 0,
    type: 0,
    top: 10,
    left: 10,
    width: 10,
    height: 10,
    fontType: '',
    fontColor: '',
    isShow: false,
    fontSize: 12,
    fontStyle:'',
    hintText:''
  };

}

