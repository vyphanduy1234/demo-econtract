import { SettingController } from './../../models/setting';
import { SettingComponent } from './../setting/setting.component';
import {
  Component,
  ComponentFactory,
  ComponentFactoryResolver,
  HostListener,
  Input,
  OnInit,
  Output,
  ViewChild,
  EventEmitter,
} from '@angular/core';

@Component({
  selector: 'app-field',
  templateUrl: './field.component.html',
  styleUrls: ['./field.component.css'],
})
export class FieldComponent implements OnInit {
  title = 'demo-econtract';
  x: number;
  y: number;
  px: number; //pointer x
  py: number; //pointer y
  fontSize: number;
  width: number;
  height: number;
  minArea: number;
  draggingCorner: boolean;
  draggingWindow: boolean;
  resizer: Function;
  isMouseOver: Boolean;
  isMoving: Boolean;
  isVisible: Boolean;

  @Output() onRemove = new EventEmitter<number>();
  @Output() onComponentUpdate = new EventEmitter<SettingController>();
  @Input() item: any;
  constructor(private resolver: ComponentFactoryResolver) {
    this.setting.left = 500;
    this.setting.top = 100;
    this.px = 0;
    this.py = 0;
    this.setting.width = 120;
    this.setting.height = 20;
    this.draggingCorner = false;
    this.draggingWindow = false;
    this.minArea = 300;
    this.fontSize = 20;
    this.isMouseOver = false;
    this.isMoving = false;
    this.isVisible = false;
  }

  loadSetting() {
    if (this.item) {
      this.setting.id = this.item.id;
      this.setting.type = this.item.type;
      this.setting.left = this.item.left;
      this.setting.top = this.item.top;
      this.setting.width = this.item.width;
      this.setting.height = this.item.height;
      this.setting.fontColor = this.item.fontColor;
      this.setting.fontType = this.item.fontType;
      this.setting.fontStyle = this.item.fontStyle;
      this.setting.fontSize = this.item.fontSize;
      this.setting.hintText = this.item.hintText;
    }
  }

  setting: SettingController = {
    id: 0,
    type: 0,
    fontType: '',
    isShow: false,
    top: 0,
    left: 0,
    width: 0,
    height: 0,
    fontColor: '',
    fontSize: 12,
    fontStyle:'',
    hintText: ''
  };
  @ViewChild(SettingComponent, { static: false }) itemModal;

  onModalEventEmmit(setting) {
    this.applySetting(setting);
  }

  openSetting() {
    this.itemModal.prepareCurrentSetting(this.setting);
  }

  applySetting(setting) {
    console.log("setting:" + JSON.stringify(setting));

    this.setting = setting;
    this.onComponentUpdate.emit(this.setting);
  }

  ngOnInit() {
    this.loadSetting();
  }

  area() {
    return this.setting.width * this.setting.height;
  }

  onTextBlur(){
    console.log(this.setting);

    this.onComponentUpdate.emit(this.setting);
  }

  onWindowPress(event: MouseEvent) {
    this.draggingWindow = true;
    this.px = event.clientX;
    this.py = event.clientY;
    console.log(`onWindowPress : x: ${this.px} y: ${this.py}`);
  }

  onWindowDrag(event: MouseEvent) {
    if (!this.draggingWindow) {
      return;
    }
    this.isMoving = true;
    let offsetX = event.clientX - this.px;
    let offsetY = event.clientY - this.py;

    this.setting.left += offsetX;
    this.setting.top += offsetY;
    this.px = event.clientX;
    this.py = event.clientY;
  }

  bottomRightResize(offsetX: number, offsetY: number) {
    this.setting.width += offsetX;
    this.setting.height += offsetY;
  }

  topResize(offsetX: number, offsetY: number) {
    this.setting.top += offsetY;
    this.setting.height -= offsetY;
  }

  leftResize(offsetX: number, offsetY: number) {
    this.setting.left += offsetX;
    this.setting.width -= offsetX;
  }

  bottomResize(offsetX: number, offsetY: number) {
    this.setting.height += offsetY;
  }

  rightResize(offsetX: number, offsetY: number) {
    this.setting.width += offsetX;
  }

  onCornerClick(event: MouseEvent, resizer?: Function) {
    this.draggingCorner = true;
    this.px = event.clientX;
    this.py = event.clientY;
    this.resizer = resizer;
    event.preventDefault();
    event.stopPropagation();
  }

  @HostListener('document:mousemove', ['$event'])
  onCornerMove(event: MouseEvent) {
    if (!this.draggingCorner) {
      return;
    }
    let offsetX = event.clientX - this.px;
    let offsetY = event.clientY - this.py;

    let lastX = this.setting.left;
    let lastY = this.setting.top;
    let pWidth = this.setting.width;
    let pHeight = this.setting.height;

    this.resizer(offsetX, offsetY);
    if (this.area() < this.minArea) {
      this.setting.left = lastX;
      this.setting.top = lastY;
      this.setting.width = pWidth;
      this.setting.height = pHeight;
    }
    this.px = event.clientX;
    this.py = event.clientY;
  }

  @HostListener('document:mouseup', ['$event'])
  onCornerRelease(event: MouseEvent) {
    if (this.draggingWindow || this.draggingCorner) {
      console.log("onChange" + JSON.stringify(this.setting));
      this.onComponentUpdate.emit(this.setting);
    }
    this.draggingWindow = false;
    this.draggingCorner = false;
    this.isMoving = false;
  }

  remove(position) {
    this.onRemove.emit(position);
  }
}
