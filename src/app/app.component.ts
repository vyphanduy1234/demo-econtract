import { TemplateService } from './services/template.service';
import { FieldComponent } from './components/field/field.component';
import {
  Component,
  ComponentFactory,
  ComponentFactoryResolver,
  ComponentRef,
  HostListener,
  Input,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
  ViewContainerRef,
} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'demo-econtract';

  @ViewChild('componentTarget', {
    read: ViewContainerRef,
  })
  target;

  constructor(private _templateService: TemplateService) {}
  listMetaData: any[] = [];
  fieldCount: number = 0;

  addComponent() {
    let component: any = {
      id: '0',
      type: 0,
      fontType: 'VNI',
      isShow: false,
      top: 100,
      left: 100,
      width: 80,
      height: 20,
      fontColor: 'black',
      fontSize: 12,
    };
    console.log(this.listMetaData);
    this._templateService.addTemplate(component).subscribe((newComponent) => {
    this.listMetaData.push(newComponent);
    });
  }
  updateComponent(setting) {
    this.listMetaData.forEach((item) => {
      if (item.id === setting.id) {
        Object.assign(item, setting);
        this._templateService
          .updateTemplate(setting)
          .subscribe((data) => console.log(data));
      }
    });
    console.log(this.listMetaData);
  }

  remove(position) {
    console.log(position);

    const id = this.listMetaData.indexOf(position);
    this.listMetaData.splice(id, 1);
    console.log(this.listMetaData.length);

    this._templateService
      .removeTemplate(position)
      .subscribe((data) => console.log(data));
  }

  loadTemplateData() {
    this._templateService.getAllTemplateData().subscribe((data) => {
      this.listMetaData = data;
    });
  }

  ngOnInit(): void {
    this.loadTemplateData();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    console.log(event);
  }
}
