import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TemplateService {

  constructor(private _httpClinet: HttpClient) { }

  getAllTemplateData(): Observable<any>{
    return this._httpClinet.get(API_ENDPONT);
  }

  updateTemplate(component): Observable<any>{
    return this._httpClinet.put(API_ENDPONT+ `/${component.id}` ,component);
  }

  addTemplate(component): Observable<any>{
    return this._httpClinet.post(API_ENDPONT,component);
  }

  removeTemplate(component): Observable<any>{
    return this._httpClinet.delete(API_ENDPONT+ `/${component.id}` ,component);
  }
}

export const API_ENDPONT = "https://5ff68ff7e7164b0017e1964a.mockapi.io/template/template-data"
