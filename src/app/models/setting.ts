export class SettingController {
  id: number;
  type: number;
  top: number;
  left: number;
  isShow: Boolean;
  width: number;
  height: number;
  fontType: string;
  fontColor: string;
  fontSize: number;
  fontStyle: string;
  hintText: string;
}
